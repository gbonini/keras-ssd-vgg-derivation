import cv2
import numpy as np

def draw_bb(img, text, bb_box, bb_color = (0,0,255), alpha = 0.5):
   output = img

   # draw bb around the object
   cv2.rectangle(output, bb_box[0], bb_box[1], bb_color,2)

   if(bb_box[1][0]-bb_box[0][0] <=50 and bb_box[1][1]-bb_box[0][1] <=50):
      return output

   # text parameters
   # rectangle_bgr = (0, 0, 0)
   rectangle_bgr = bb_color
   font_scale = 1.1
   font = cv2.FONT_HERSHEY_PLAIN



   text_width =[]; text_height = []
   for t in text:
      # get the width and height of the text box
      (t_width, t_height) = cv2.getTextSize(t, font, fontScale=font_scale, thickness=3)[0]
      text_width.append(t_width); text_height.append(t_height)



   # check if all text fit inside the bounding box
   val = (bb_box[1][0]-bb_box[0][0]-3)
   fit = False
   if(all(x < val for x in text_width)):
      fit = True


   height_offset = 0
   for i, (t, t_width, t_height)  in enumerate( zip(text, text_width, text_height)) :

      # set the text start position
      height_offset += 5 + t_height
      # compute the text rectangle position
      if(fit):
         text_offset_x = bb_box[0][0] + 3
         text_offset_y = bb_box[0][1] + height_offset
      else:
         text_offset_x = bb_box[1][0] + 3
         text_offset_y = bb_box[0][1] + height_offset


      # make the coords of the box with a small padding of two pixels
      box_coords = ((text_offset_x, text_offset_y+2), (text_offset_x + t_width +2 , text_offset_y - t_height -2 ))
      text_box = np.ones( (t_height+4, t_width+2, 3), np.uint8) * np.array(rectangle_bgr, np.uint8)


      output_crop  = output[ box_coords[1][1]:box_coords[0][1], box_coords[0][0]:box_coords[1][0] , :]
      # print("output_crop: ", output_crop.shape)
      # print("text_box: ", text_box.shape)
      # print("output: ", output.shape)
      # print(box_coords[0][1], box_coords[1][1], box_coords[0][0], box_coords[1][0])

      if(output_crop.shape == text_box.shape):
         cv2.addWeighted(output_crop, 0, text_box, 1, 0, output_crop)
         cv2.putText(output, t, (text_offset_x, text_offset_y), font, fontScale=font_scale, color=(255, 255, 255), thickness=1)

   return output